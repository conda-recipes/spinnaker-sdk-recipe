#!/bin/bash

DEB_FILES="\
  libspinnaker-${PKG_VERSION}_amd64.deb \
  libspinnaker-${PKG_VERSION}_amd64-dev.deb \
  libspinnaker-c-${PKG_VERSION}_amd64.deb \
  libspinnaker-c-${PKG_VERSION}_amd64-dev.deb \
  "

# Extract files from deb packages
mkdir -p out
for package in $DEB_FILES
do
  dpkg -x $package out/
done

# Fix links with incorrect case
cd out/usr/lib
rm -f libspinnaker.so.1 libspinnaker-c.so.1
ln -s libSpinnaker.so.${PKG_VERSION} libSpinnaker.so.1
ln -s libSpinnaker_C.so.${PKG_VERSION} libSpinnaker_C.so.1
cd -

# Copy lib include and share dir to conda PREFIX
cp -rp ./out/usr/* ${PREFIX}/
