# spinnaker-sdk conda recipe

Home: "https://www.flir.com/products/spinnaker-sdk"

Package license: FLIR Systems, Inc. License

Recipe license: BSD 3-Clause

Summary: Spinnaker SDK for cameras from FLIR

Package Spinnaker SDK libraries and includes files required by ADSpinnaker.
